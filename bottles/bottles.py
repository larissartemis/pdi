# -*- coding: utf-8 -*-
"""
Created on Sat Aug 31 12:59:47 2019

@author: Larissa Artemis
"""

#%% imports
import cv2 
import glob
import csv
import numpy as np 

#%% 

def escrever (img, q, w, e, r, t, y, u, i):
    with open('resultados.csv', 'a', newline='') as writeFile:
            writer = csv.writer(writeFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL, dialect='excel')
            writer.writerow([img, str(q), str(w), str(e), str(r), str(t), str(y), str(u), str(i)])
            writeFile.close()

#%% 

image_list = []
for filename in glob.glob('images/*.jpg'): 
    image_list.append(filename)

with open('resultados.csv', 'w', newline='') as writeFile:
    writer = csv.writer(writeFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL, dialect='excel')
    writer.writerow(["IMAGE", "CONTENT_HIGH", "CONTENT_LOW", "COVER_NONE", "BOTTLE_SMASHED", "LABEL_WHITE", "LABEL_MISPLACED", "LABEL_NONE", "BOTTLE_NONE"])
writeFile.close()

    
qtdeCheia = 0
qtdeVazia = 0
qtdeTemGarrafa = 0
qtdeSemTampa = 0
qtdeSemRotulo = 0
qtdeRotuloBranco = 0

qtdeSucesso = 0
    
for i in image_list:
    image = cv2.imread(i, cv2.COLOR_BGR2RGB)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 
    ret, thresh = cv2.threshold(gray, 150, 255, cv2.THRESH_BINARY_INV) 
    img_dilation = cv2.dilate(thresh, np.ones((1,1), np.uint8) , iterations=1) 
    
    # checar se está cheia demais
    roi = img_dilation[110:140, 140:220] 
#    cv2.rectangle(image, (140, 110), (220, 140), (0, 255, 0), 2)
#    cv2.imshow('roi', image)     
    blackPercentage = 100 * np.sum(roi == 0)/(roi.shape[0]*roi.shape[1])
    if blackPercentage < 40:
        qtdeCheia += 1
        qtdeSucesso += 1
        escrever(i, 1, 0, 0, 0, 0, 0, 0, 0)
    
    # checar se a garrafa está lá     
    roi = img_dilation[1:250, 135:225] 
#    cv2.rectangle(image, (135, 1), (225, 250), (0, 255, 0), 2)
#    cv2.imshow('roi', image)     
    blackPercentage = 100 * np.sum(roi == 0)/(roi.shape[0]*roi.shape[1])
    if blackPercentage > 90:
        qtdeTemGarrafa += 1
        qtdeSucesso += 1
        escrever(i, 0, 0, 0, 0, 0, 0, 0, 1)
    
    # checar se a garrafa tem tampa
    roi = img_dilation[5:45, 150:200] 
#    cv2.rectangle(image, (150, 5), (200, 45), (0, 255, 0), 2)
#    cv2.imshow('roi', image)  
    blackPercentage = 100 * np.sum(roi == 0)/(roi.shape[0]*roi.shape[1])
    if blackPercentage > 60 and blackPercentage < 100:
        qtdeSemTampa += 1
        qtdeSucesso += 1
        escrever(i, 0, 0, 1, 0, 0, 0, 0, 0)
    
    # checar se o rótulo é branco
    roi = img_dilation[180:280, 110:240] 
#    cv2.rectangle(image, (110, 180), (240, 280), (0, 255, 0), 2)
#    cv2.imshow('roi', image)  
    blackPercentage = 100 * np.sum(roi == 0)/(roi.shape[0]*roi.shape[1])
    if blackPercentage < 10:
        qtdeRotuloBranco += 1
        qtdeSucesso += 1
        escrever(i, 0, 0, 0, 0, 1, 0, 0, 0)

    # checar se está vazia demais    
    roi = img_dilation[110:140, 140:220] 
#    cv2.rectangle(thresh, (140, 110), (220, 140), (0, 255, 0), 2)
#    cv2.imshow('roi', thresh)    
    # varios falsos positivos eram quando não havia nenhuma garrafa, a dupla condição evita isso
    roi_tem_garrafa = img_dilation[1:250, 135:225] 
    blackPercentage_tem_garrafa = 100 * np.sum(roi_tem_garrafa == 0)/(roi_tem_garrafa.shape[0]*roi_tem_garrafa.shape[1])
    if blackPercentage_tem_garrafa < 90:
        blackPercentage = 100 * np.sum(roi[:] == 0)/(roi.shape[0]*roi.shape[1])
        if blackPercentage > 95:
            qtdeVazia += 1
            qtdeSucesso += 1
            escrever(i, 0, 1, 0, 0, 0, 0, 0, 0)

    # checar se tem rótulo 
    roi = img_dilation[180:280, 110:240] 
#    cv2.rectangle(image, (110, 180), (240, 280), (0, 255, 0), 2)
#    cv2.imshow('roi', image)  
    # varios falsos positivos eram quando não havia nenhuma garrafa, a dupla condição evita isso
    roi_tem_garrafa = img_dilation[1:250, 135:225] 
    blackPercentage_tem_garrafa = 100 * np.sum(roi_tem_garrafa == 0)/(roi_tem_garrafa.shape[0]*roi_tem_garrafa.shape[1])
    if blackPercentage_tem_garrafa < 90:
        blackPercentage = 100 * np.sum(roi == 0)/(roi.shape[0]*roi.shape[1])
        if blackPercentage < 5:
            qtdeSemRotulo += 1
            qtdeSucesso += 1
            escrever(i, 0, 0, 0, 0, 0, 0, 1, 0)
    
#%% resumo

print("---------------------------------------------------")
print("erros identificados ", qtdeSucesso)
print("\n")
print("garrafas cheias demais", qtdeCheia)
print("garrafas vazias demais", qtdeVazia)
print("garrafas sem tampas", qtdeSemTampa)
print("garrafas sem rótulo", qtdeSemRotulo)
print("garrafas com o rótulo em branco", qtdeRotuloBranco)
print("não tem garrafa", qtdeTemGarrafa)

#%% 
cv2.destroyAllWindows()
