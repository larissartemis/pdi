# -*- coding: utf-8 -*-
"""
Created on Sun Jun  9 17:38:37 2019

@author: Larissa Artemis
"""
#%%

import random

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from keras.models import Sequential
from keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPooling2D

#%%

# Download da base de dados do Mnist 
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()

# Redimensionando o array para 4 dimensões, como é necessário pra usar o Keras 
x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)
input_shape = (28, 28, 1)

# Garantindo que os dados são float, pra não dar problema com as divisões
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')

# Normalizando os códigos RGB
x_train /= 255
x_test /= 255

# Criando a CNN
model = Sequential()

model.add(Conv2D(28, kernel_size=(3,3), input_shape=input_shape))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten()) 
model.add(Dense(128, activation=tf.nn.relu))
model.add(Dropout(0.2))
model.add(Dense(10,activation=tf.nn.softmax))

model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
model.fit(x=x_train,y=y_train, epochs=5)

test_loss, test_acc = model.evaluate(x_test, y_test)

#%% Testando...

fig = plt.figure()

for i in range(1, 5):
    image_index = random.randint(1,10000)
    ax = plt.subplot(1, 4, i)
    plt.imshow(x_test[image_index].reshape(28, 28),cmap='Greys')
    pred = model.predict(x_test[image_index].reshape(1, 28, 28, 1))
    ax.set_title(pred.argmax())
    
#%% Encontrando as imagens classificadas erradas
    
pred_labels = model.predict_classes(x_test)
pred_scores = model.predict(x_test)
idxs = ~(pred_labels == y_test)
ind = np.where(idxs)[0]
for i in ind:
        img = x_test[i]
        plt.imshow(255 - img[:, :, 0], cmap = 'gray')
        plt.xlabel('Class = ' + str(y_test[i]) + ' pred.class = ' + str(pred_labels[i]))
        plt.show()