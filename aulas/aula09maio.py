# -*- coding: utf-8 -*-

# carregar imagens em uma janela distinta do console:
# ferramentas > preferencias > console ipython > graficos > saída gráfica > qt5

#%% imports
import cv2
import matplotlib.pyplot as plt
import numpy as np

#%% boa prática!

folder = '/home/larissa/db/'
import os
os.path.join(folder, 'imagename.jpg')

#%% carregando uma imagem em escala de cinza
img = cv2.imread('downloaded.jpeg', cv2.IMREAD_GRAYSCALE)

# mostrar a imagem usando opencv
# cv2.imshow('img', img)
# cv2.waitKey(1)

# mostrar a imagem usando pyplot
plt.imshow(img, cmap='gray')

#%% carregando uma imagem colorida
img = cv2.imread('downloaded.jpeg', cv2.IMREAD_COLOR)
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.imshow(img)

#%% visualizando os tons de RGB na imagem
rgb = cv2.split(img)
plt.subplot(221), plt.title('RGB'), plt.imshow(img)
plt.subplot(222), plt.title('R'), plt.imshow(rgb[0], cmap='gray')
plt.subplot(223), plt.title('G'), plt.imshow(rgb[1], cmap='gray')
plt.subplot(224), plt.title('B'), plt.imshow(rgb[2], cmap='gray')
plt.show()

#%% distribuição de uma cor da imagem cinza
img = cv2.imread('downloaded.jpeg', cv2.IMREAD_GRAYSCALE)
plt.subplot(121), plt.title('Original'), plt.imshow(img, cmap='gray')
# a função ravel serve pra vetorizar a imagem
plt.subplot(122), plt.title('Histogram'), plt.hist(img.ravel(), 256, [0, 255]) 
plt.show()

#%% distribuição de cor de uma imagem colorida
img = cv2.imread('downloaded.jpeg')
color = ('b', 'g', 'r')
for i, col in enumerate(color):
    histr = cv2.calcHist([img], [i], None, [256], [0, 256])
    plt.plot(histr, color = col)
    plt.xlim([0, 256])
plt.show()

#%% criando imagens
img = 127*np.ones((50, 50), dtype='uint8') # 1 = preto, 255 = branco
plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.show() # ?

#%% criando imagens aleatorias uniformemente distribuidas
img = 127*np.ones((250, 250), dtype=np.uint8) 
cv2.randu(img, 0, 255) # o endereçamento da imagem aqui é por referencia!
plt.subplot(121), plt.title('Original'), plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.subplot(122), plt.title('Histogram'), plt.hist(img.ravel(), 256, [0, 255]) 
plt.show()