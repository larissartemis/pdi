# -*- coding: utf-8 -*-
"""

Spyder Editor

"""

#%% import modules

import keras
keras.__version__
import matplotlib.pyplot as plt
import numpy as np

#%% listing 5.1 instantiating small convnet

from keras import layers
from keras import models

# no modelo sequencial, as camadas são colocadas, em sequência, uma depois da outra
model = models.Sequential()
# 32 = número de neurônios, 'relu' = unidade de retificação linear
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(28, 28, 1)))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))

model.summary()

#%% listing 5.2 adding a classifier on top of the convnet

model.add(layers.Flatten())
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(10, activation='softmax'))

model.summary()

#%% listing 5.3 training the convnet on mnist image

from keras.datasets import mnist
from keras.utils import to_categorical

idx = 10
(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

plt.imshow(255 - train_images[idx], cmap = 'gray')
plt.xlabel('Class = ' + str(train_labels[idx]))

# plt.show()

train_images = train_images.reshape((60000, 28, 28, 1))
train_images = train_images.astype('float32')/255

test_images = test_images.reshape((10000, 28, 28, 1))
test_images = test_images.astype('float32')/255

train_images = to_categorical(train_images)
test_images = to_categorical(test_images)

model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

#%% train and save model

model.fit(train_images, train_labels, epochs=5, batch_size=64)
model.save('model1-5-1.h5')

from keras.models import load_model

model = load_model('model1-5-1.h5')
test_loss, test_acc = model.evaluate(test_images, test_labels)
test_acc


#%% investigate some test samples

pred_labels = model.predict_classes(test_images)
pred_scores = model.predict(test_images)
labels = np.argmax(test_labels, axis = 1)
idxs = ~(pred_labels == labels)
ind = np.where(idxs)[0]
for i in ind:
        img = test_images[i]
        plt.imshow(255 - img[:, :, 0], cmap = 'gray')
        plt.xlabel('Class = ' + str(labels[i]) + ' pred.class = ' + str(pred_labels[i]))
        plt.show()