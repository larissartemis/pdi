#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  4 13:57:03 2019

@author: alunoic
"""
#%% 
import cv2
import numpy as np
import matplotlib.pyplot as plt

# classificadores
face_class = cv2.CascadeClassifier("/media/alunoic/ESD-ISO/PDI/haarcascade_frontalface_default.xml")
olho_class = cv2.CascadeClassifier("/media/alunoic/ESD-ISO/PDI/haarcascade_eye.xml")

#%% imagem estática

# imagem
imagem = cv2.cvtColor(cv2.imread("/media/alunoic/ESD-ISO/PDI/lena.png"), cv2.COLOR_BGR2RGB)
cinza = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)

# detecção de faces
faces = face_class.detectMultiScale(
    cinza,
    scaleFactor=1.1,
    minNeighbors=5,
    minSize=(30, 30),
    flags=cv2.CASCADE_SCALE_IMAGE
)

olhos = olho_class.detectMultiScale(
    cinza,
    scaleFactor=1.1,
    minNeighbors=5,
    minSize=(10, 10),
    flags=cv2.CASCADE_SCALE_IMAGE
)

# desenhando retangulos na face e nos olhos
for (x, y, w, h) in faces:
    cv2.rectangle(imagem, (x, y), (x+w, y+h), (0, 255, 0), 2)
    
for (x, y, w, h) in olhos:
    cv2.rectangle(imagem, (x, y), (x+w, y+h), (255, 0, 0), 2)

# resultado
plt.title("Faces found")
plt.imshow(imagem)

#%% vídeo

# video
cap = cv2.VideoCapture("chaplin.mp4")

if (cap.isOpened()== False): 
  print("Erro ao abrir o arquivo")

while(cap.isOpened()):
    # lendo cada frame e criando uma imagem cinza com isso
    ret, frame = cap.read()
    cinza = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # detectando as faces e os olhos
    faces = face_class.detectMultiScale(
        cinza,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    olhos = olho_class.detectMultiScale(
        cinza,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(10, 10),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    imagem = cv2.cvtColor(cinza, cv2.COLOR_GRAY2RGB)
    
    # desenhando retangulos na face e nos olhos
    for (x, y, w, h) in faces:
        cv2.rectangle(imagem, (x, y), (x+w, y+h), (0, 255, 0), 2)
    
    for (x, y, w, h) in olhos:
        cv2.rectangle(imagem, (x, y), (x+w, y+h), (255, 0, 0), 2)

    # resultado
    plt.imshow(imagem)

cap.release()
cv2.destroyAllWindows()