#%%

import sys
print(sys.version)

import cv2
print('OpenCV version = ' + cv2.__version__)

import tensorflow as tf
import numpy as np

#%%

img = np.random.random((200, 300))
print("Image type = " + str(img.dtype))

#%%

cv2.namedWindow('img', cv2.WINDOW_KEEPRATIO)
cv2.imshow('img', img)
while True:
        if 0xFF & cv2.waitKey(1) == ord('q'):
            break
cv2.destroyAllWindows()

#%%

import matplotlib.pyplot as plt

plt.imshow(img, cmap = 'gray')
plt.show