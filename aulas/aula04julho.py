# -*- coding: utf-8 -*-
"""
Created on Sat Jul  6 12:59:02 2019

@author: Larissa Artemis
"""

#%% 

import cv2
import numpy as np
import imutils

from pathlib import Path
img_folder = Path("D:\workspace\pdi\imagens")

#%% Detectar features numa imagem usando SIFT
# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_feature2d/py_sift_intro/py_sift_intro.html

file_to_open = img_folder / "torre-eiffel.jpg"

img = cv2.imread(str(file_to_open))
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

sift = cv2.xfeatures2d.SIFT_create()
kp = sift.detect(gray, None)

img_out = cv2.drawKeypoints(gray, kp, gray)
file_to_save = img_folder / "keypoints.jpg"
cv2.imwrite(str(file_to_save), img_out)

img = cv2.drawKeypoints(gray, kp, img_out, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
cv2.imwrite(str(file_to_save), img_out)

#%% Realizar a detecção de objetos numa imagem
# https://www.quora.com/How-can-I-detect-an-object-from-static-image-and-crop-it-from-the-image-using-openCV

file_to_open = img_folder / "objetos.jpg"

image = cv2.imread(str(file_to_open))
edged = cv2.Canny(image, 10, 250)
# cv2.imshow("Edges", edged)
# cv2.waitKey(0)
 
#applying closing function 
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
# cv2.imshow("Closed", closed)
# cv2.waitKey(0)
 
#finding_contours 
(_, cnts, _) = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
 
for c in cnts:
	peri = cv2.arcLength(c, True)
	approx = cv2.approxPolyDP(c, 0.02 * peri, True)
	cv2.drawContours(image, [approx], -1, (0, 255, 0), 2)
cv2.imshow("Output", image)
cv2.waitKey(0)


#%% Montar um panorama contendo 2 imagens
# https://stackoverflow.com/questions/34362922/how-to-use-opencv-stitcher-class-with-python

img1 = img_folder / "img-1.jpg"
img2 = img_folder / "img-2.jpg"

output = img_folder / "img-12.jpg"

stitcher = cv2.createStitcher(False)
img_left = cv2.imread(str(img1))
img_right = cv2.imread(str(img2))
result = stitcher.stitch((img_left, img_right))

cv2.imwrite(str(output), result[1])
cv2.imshow("Output", result[1])
cv2.waitKey(0)

#%% Montar um panorama contendo mais de 3 imagens
# https://www.pyimagesearch.com/2018/12/17/image-stitching-with-opencv-and-python/

img1 = img_folder / "img-1.jpg"
img2 = img_folder / "img-2.jpg"
img3 = img_folder / "img-3.jpg"

output = img_folder / "img-123.jpg"

img1 = cv2.imread(str(img1))
img2 = cv2.imread(str(img2))
img3 = cv2.imread(str(img3))

images = [img1, img2, img3]

stitcher = cv2.createStitcher() 
(status, stitched) = stitcher.stitch(images)

cv2.imwrite(str(output), stitched)
cv2.imshow("Stitched", stitched)
cv2.waitKey(0)

#%% Realizar a reconstrução 3D a partir de imagens de um objeto usando o COLMAP












































