import cv2
import matplotlib.pyplot as plt
import numpy as np

image = cv2.imread('/home/alunoic/Downloads/lena.png')
image_scaled = cv2.resize(image, None, fx = 0.75, fy = 0.75)
cv2.imshow('Scaling linear interpolation', image_scaled)
cv2.waitKey()
cv2.destroyAllWindows()