#%%

import numpy as np
import cv2
import matplotlib.pyplot as plt

#%%

img1 = cv2.imread("/home/alunoic/lena.png")
img2 = cv2.imread("/home/alunoic/macaco.jpg")

# img3 = img2 + img1 
# normalizar?
# img3 = cv2.add(img1, img2) 
# com peso?
# img3 = cv2.addWeighted(img1, 0.2, img2, 0.8, 0)
# outra forma de somar as imagens
# img1 = img1.astype('float32')/255.0
# img2 = img2.astype('float32')/255.0
# img3 = 0.2 * img1 + 0.8 * img2;

# img3 = img2 + 100 
# somar a todos os canais sem saturar?
# const = 100
# img = np.zeros((img2.shape[0], img2.shape[1], img2.shape[2]), np.uint8)
# img[:] = (const, const, const)
# img3 = img2 + img 

# img3 = cv2.max(img1, img2) 
# img3 = cv2.absdiff(img1, img2) 

# somar imagem com ruido gaussiano
media = 127
desvio = 30
img = np.zeros((img2.shape[0], img2.shape[1], img2.shape[2]), np.uint8)
bgr = cv2.split(img)
cv2.randn(bgr[0], media, desvio) 
cv2.randn(bgr[1], media, desvio)
cv2.randn(bgr[2], media, desvio)
img = cv2.merge(bgr)
img2 = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
img3 = cv2.add(img1, img2)

plt.subplot('131'), plt.title('img1')
plt.imshow(cv2.cvtColor(img1, cv2.COLOR_BGR2RGB))

plt.subplot('132'), plt.title('img2')
plt.imshow(cv2.cvtColor(img2, cv2.COLOR_BGR2RGB))

plt.subplot('133'), plt.title('img3')
plt.imshow(cv2.cvtColor(img3, cv2.COLOR_BGR2RGB))
