# -*- coding: utf-8 -*-

import numpy as np
import cv2
import matplotlib.pyplot as plt

folder = '/home/alunoic/'

#%% criando uma imagem colorida com valores aleatorios 

img = np.ones((250, 250, 3), dtype = np.uint8)
bgr = cv2.split(img)
cv2.randu(bgr[0], 0, 255)
cv2.randu(bgr[1], 0, 255)
cv2.randu(bgr[2], 0, 255)
img = cv2.merge(bgr)
# opencv usa bgr e o plt usa rgb, lembrar de ajustar os canais
img2 = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.imshow(img)

#%% criando uma imagem colorida com valores aleatorios baseado numa distribuicao gaussiana

img = np.ones((250, 250, 3), np.uint8)
bgr = cv2.split(img)
cv2.randn(bgr[0], 127, 40) # parametros = 127 media, 40 desvio padrao
cv2.randn(bgr[1], 127, 40)
cv2.randn(bgr[2], 127, 40)
img = cv2.merge(bgr)
img2 = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

plt.subplot(211), plt.imshow(img)
plt.subplot(212)
color = ('r', 'g', 'b')
for i, col in enumerate (color):
    hist = cv2.calcHist([img], [i], None, [256], [0, 256])
    plt.plot (hist, color = col)
    plt.xlim([0,256])
plt.show()

#%% convert images to different ranges

# gerando um array aleatorio usando numpy
np.random.seed(0)
img = np.random.random((3,3))

# gerando um array aleatorio usando opencv
img = np.ones((3,3),dtype=np.float32)
cv2.randn(img, 0, 1)

# normalizacao
# entre 0 e 1 (z-zmin)/(zmax-zmin)
# uint8 (255*(z-zmin)/(zmax-zmin))
print('Original = \n', img, '\n\n')
cv2.normalize(img, img, 255, 0, cv2.NORM_MINMAX)
print('Normalized = \n', img, '\n\n')
img = np.asarray(img, dtype = np.uint8)
print('Converted to uint8 = \n', img, '\n\n')

#%%

img = cv2.imread('/home/alunoic/lenna.png')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
h, w = img.shape[:2]
quarter_height, quarter_width = h/4, w/4
T = np.float32([
        [1, 0, quarter_width], 
        [0, 1, quarter_height], 
        ])
img_translation = cv2.warpAffine(img, T, (w, h))
plt.subplot(211), plt.imshow(img)
plt.subplot(212), plt.imshow(img_translation)
plt.show()
