# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 09:53:39 2019

@author: Larissa Artemis
"""

# imagens 
# https://www.kaggle.com/c/dogs-vs-cats-redux-kernels-edition/data

#%% imports

import numpy as np
import matplotlib.pyplot as plt
import cv2, os

from keras.models import Sequential
from keras.layers import Activation, Dropout, Flatten, Conv2D, MaxPooling2D, Dense
from keras.optimizers import RMSprop

#%% 

TRAIN_DIR = 'D:/workspace/pdi/cats_vs_dogs/train/'
TEST_DIR = 'D:/workspace/pdi/cats_vs_dogs/test/'
ROWS = 64
COLS = 64
CHANNELS = 3

#%% processamento das imagens

def read_image(file_path):
    img = cv2.imread(file_path, cv2.IMREAD_COLOR)
    img = cv2.resize(img, (ROWS, COLS), interpolation=cv2.INTER_CUBIC)
   
    return img

def prep_data(images):
    count = len(images)
    data = np.ndarray((count, CHANNELS, ROWS, COLS), dtype=np.uint8)

    for i, image_file in enumerate(images):
        image = read_image(image_file)
        data[i] = image.T  # transpose mxn --> nxm
        if i%250 == 0: print('Processed {} of {}'.format(i, count))
    
    return data

#%% separando cães de gatos (e colocando um label pra isso)

train_images = [TRAIN_DIR + i for i in os.listdir(TRAIN_DIR)] 
train = prep_data(train_images)
    
labels = []
for i in train_images:
    if 'dog.' in i:
        labels.append(1)
    else:
        labels.append(0)

test_images = [TEST_DIR + i for i in os.listdir(TEST_DIR)]
test = prep_data(test_images)

    
#%% criando o modelo

model = Sequential()

model.add(Conv2D(32, (3, 3), padding='same', input_shape=(CHANNELS, ROWS, COLS), activation='relu'))
model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
model.add(MaxPooling2D(data_format="channels_first", pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(MaxPooling2D(data_format="channels_first", pool_size=(2, 2)))

model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
model.add(MaxPooling2D(data_format="channels_first", pool_size=(2, 2)))

model.add(Conv2D(256, (3, 3), padding='same', activation='relu'))
model.add(Conv2D(256, (3, 3), padding='same', activation='relu'))
model.add(MaxPooling2D(data_format="channels_first", pool_size=(2, 2)))

model.add(Flatten()) 
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

opt = RMSprop(lr=1e-4)
model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
model.fit(train, labels, epochs = 25)

#%% testando

predictions = model.predict(test)
for i in range(0,25):
    if predictions[i, 0] >= 0.5: 
        print('I am {:.2%} sure this is a Dog'.format(predictions[i][0]))
    else: 
        print('I am {:.2%} sure this is a Cat'.format(1-predictions[i][0]))
        
    plt.imshow(test[i].T)
    plt.show()
