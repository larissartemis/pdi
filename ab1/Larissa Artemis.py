#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Processamento Digital de Imagens
Instituto de Computação - UFAL
AB1 - 25/06/2019

Larissa Artemis Luna Monteiro
"""

# %%===========================================================================
# Import packages
# =============================================================================
import cv2
import numpy as np
import matplotlib.pyplot as plt

# %%===========================================================================
# Q1 Carregue a imagem 'maceio.jpg' e execute o que se pede:
img = cv2.imread('maceio.jpg', cv2.IMREAD_COLOR)

# (a) Imagem original.
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

# (b) Imagem "achatada", de altura igual a 25% da altura da imagem original.
achatada = cv2.resize(img, None, fx = 1, fy = 0.25)

# (c) Imagem "cortada", destacando um dos barcos.
cortada = img[0:750, 0:300]

# (d) Imagem rotacionada de 60 no sentido anti-horário.
angle = 60
image_center = tuple(np.array(img.shape[1::-1]) / 2)
rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
rotacionada = cv2.warpAffine(img, rot_mat, img.shape[1::-1], flags=cv2.INTER_LINEAR)

# Espera-se um resultado semelhante ao da imagem 'q1.png'.
plt.subplot(221), plt.title('a'), plt.imshow(img)
plt.subplot(222), plt.title('b'), plt.imshow(achatada)
plt.subplot(223), plt.title('c'), plt.imshow(cortada)
plt.subplot(224), plt.title('d'), plt.imshow(rotacionada)
plt.show()
# =============================================================================

# %%===========================================================================
# Q2a Desenvolver uma função que gera a imagem de um tabuleiro quadriculado 
# semelhante ao ilustrado na imagem 'ckbd.png'.
# A funcão deve receber 4 argumentos de entrada:
# - rows = Número de linhas da imagem
# - cols = Número de colunas da imagem
# - nr = Número de linhas para um dado valor (preto ou branco)
# - nc = Número de colunas para um dado valor (preto ou branco).
#
# Exemplo: A imagem 'ckbd.png' foi obtida com os seguintes valores:
#    rows = 40
#    cols = 50
#    nr = 10
#    nc = 5

rows = 40
cols = 50
nr = 10
nc = 5

tab = np.ones((rows, cols), dtype='uint8') 

for r in range(0, rows):
    for c in range(0, cols):
        conta1 = (c/nc) % 2
        conta2 = (r/nr) % 2
        
        if (conta2 >= 1):
            if (conta1 >= 1):
                tab[r, c] = 1
            else:
                tab[r, c] = 255
        else:
            if (conta1 >= 1):
                tab[r, c] = 255
            else:
                tab[r, c] = 1
        
plt.imshow(tab, cmap='gray')
plt.show() 
# =============================================================================

# %%===========================================================================
# Q2b Carregar a imagem 'ckbd2.png' que contém um tabuleiro com uma iluminação
# que vai gradativamente do escuro ao claro. Desenvolver o código que retira 
# esta iluminação e resulte numa imagem como a mostrada no arquivo 
# 'ckbd3.png'.

ckbd = cv2.imread('ckbd2.png', cv2.IMREAD_COLOR)
ckbd = cv2.cvtColor(ckbd, cv2.COLOR_BGR2RGB)

iluminacao = np.zeros((ckbd.shape[0], ckbd.shape[1], ckbd.shape[2]), dtype='uint8') 
resultado = np.zeros((ckbd.shape[0], ckbd.shape[1], ckbd.shape[2]), dtype='uint8') 

rows = 40
cols = 50

passo = 255 / cols

#aux = 1
#for c in range(0, cols):
#    for r in range(0, rows):
#        iluminacao[r, c] = aux
#    aux += passo    
#resultado = cv2.subtract(ckbd, iluminacao)

aux = 255
for c in range(0, cols):
    for r in range(0, rows):
        iluminacao[r, c] = aux
    aux -= passo
resultado = cv2.add(ckbd, iluminacao)
            
plt.subplot(131), plt.imshow(ckbd)
plt.subplot(132), plt.imshow(iluminacao)
plt.subplot(133), plt.imshow(resultado)
plt.show() 

# =============================================================================

# %%===========================================================================
# Q3 Defina uma função que receba dois argumentos de entrada:
# - img = Uma imagem de um canal.
# - n = Número de níveis de intensidade da imagem de saída.
# A função deve retornar a imagem de entrada com um número 'n' de níveis de 
# cinza. Aplicar o código à imagem 'brain-mri.jpg' que deve resultar na imagem
# 'brain2.jpg'.
# =============================================================================

# %%===========================================================================
# Q4 Calcule e normalize a imagem integral da imagem 'rectangle.jpg'
# =============================================================================

# %%===========================================================================
# Q5 Detectar todas as faces na imagem 'faces.jpg'.

face_class = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

imagem = cv2.cvtColor(cv2.imread("faces.jpg"), cv2.COLOR_BGR2RGB)
cinza = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)

faces = face_class.detectMultiScale(
    cinza,
    scaleFactor=1.1,
    minNeighbors=5,
    minSize=(20, 20),
    flags=cv2.CASCADE_SCALE_IMAGE
)

for (x, y, w, h) in faces:
    cv2.rectangle(imagem, (x, y), (x+w, y+h), (0, 255, 0), 2)
    
plt.imshow(imagem)

# =============================================================================
